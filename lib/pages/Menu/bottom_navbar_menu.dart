import 'package:flutter/material.dart';
import 'package:m_health/pages/Menu/detail_menu.dart';
import 'package:m_health/pages/Menu/reminder_menu.dart';

class BottomNavbarMenu extends StatefulWidget {
  @override
  _BottomNavbarMenuState createState() => _BottomNavbarMenuState();
}

class _BottomNavbarMenuState extends State<BottomNavbarMenu> {
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    Widget body() {
      switch (currentIndex) {
        case 0:
          return ReminderMenu();
          break;
        case 1:
          return DetailMenu();
          break;
        default:
          return ReminderMenu();
      }
    }

    Widget customButtomNavbarJkn() {
      return BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        clipBehavior: Clip.antiAlias,
        child: BottomNavigationBar(
          currentIndex: currentIndex,
          onTap: (value) {
            setState(() {
              currentIndex = value;
            });
          },
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 20, bottom: 10),
                child: Icon(
                  Icons.date_range_outlined,
                  size: 30,
                  color: currentIndex == 0 ? Color(0xFF00CACD) : Colors.grey,
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 20, bottom: 10),
                child: Icon(
                  Icons.description_outlined,
                  size: 30,
                  color: currentIndex == 1 ? Color(0xFF00CACD) : Colors.grey,
                ),
              ),
              label: '',
            ),
          ],
        ),
      );
    }

    return Scaffold(
      bottomNavigationBar: customButtomNavbarJkn(),
      body: body(),
    );
  }
}
