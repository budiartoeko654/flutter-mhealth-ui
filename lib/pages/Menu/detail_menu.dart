import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';
import 'package:m_health/widgets/list_info_puskesmas.dart';

class DetailMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF00CACD),
        title: Text(
          "Detail",
          style: defaultTextFont.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      );
    }

    Widget containerPhoto() {
      return Container(
        padding: EdgeInsets.only(bottom: 25),
        width: double.infinity,
        height: 235,
        decoration: BoxDecoration(
          color: Color(0xFF00CACD),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(top: 30),
              width: 120,
              height: 120,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/hospital.png"),
                    fit: BoxFit.cover),
              ),
            ),
          ],
        ),
      );
    }

    Widget listDetailPuskesmas() {
      return Container(
        margin: EdgeInsets.only(
          left: defaultMargin,
          right: defaultMargin,
          top: 25,
        ),
        child: Column(
          children: [
            ListInfoPuskesmas("Nama Puskesmas", "Pulo Merak"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("Jumlah KK", "8868.00"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("Jumlah KK Pra-Sehat", "6212.00"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("Jumlah KK Tidak Sehat", "1322.00"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("Jumlah KK Sehat", "1334.00"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("IKS Inti", "0.15"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("Presentase Indikator", "52.06"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("Jumlah Y", "4617.00"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("Jumlah T", "4251.00"),
            SizedBox(
              height: 15,
            ),
            ListInfoPuskesmas("jumlah N", "0.00"),
            SizedBox(
              height: 15,
            ),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: Color(0xFFF6F7F9),
      appBar: header(),
      body: ListView(
        children: [
          containerPhoto(),
          listDetailPuskesmas(),
        ],
      ),
    );
  }
}
