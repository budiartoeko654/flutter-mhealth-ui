import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';

class ReminderMenu extends StatelessWidget {
  Widget header() {
    return AppBar(
      elevation: 0,
      backgroundColor: Color(0xFF00CACD),
      title: Text(
        "Reminder",
        style: defaultTextFont.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
      centerTitle: true,
    );
  }

  Widget tableJkn() {
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          columns: <DataColumn>[
            DataColumn(
                label: Text("No",
                    style:
                        defaultTextFont.copyWith(fontWeight: FontWeight.bold))),
            DataColumn(
                label: Text("Kelurahan",
                    style:
                        defaultTextFont.copyWith(fontWeight: FontWeight.bold))),
            DataColumn(
                label: Text("Rw/Rt",
                    style:
                        defaultTextFont.copyWith(fontWeight: FontWeight.bold))),
            DataColumn(
                label: Text("Nama KK",
                    style:
                        defaultTextFont.copyWith(fontWeight: FontWeight.bold))),
          ],
          rows: <DataRow>[
            DataRow(
              cells: <DataCell>[
                DataCell(Text("1.", style: defaultTextFont.copyWith())),
                DataCell(Text("TamanSari", style: defaultTextFont.copyWith())),
                DataCell(Text("1/2", style: defaultTextFont.copyWith())),
                DataCell(Text("Supardi", style: defaultTextFont.copyWith())),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text("2.", style: defaultTextFont)),
                DataCell(Text("TamanSari", style: defaultTextFont)),
                DataCell(Text("1/2", style: defaultTextFont)),
                DataCell(Text("Sulasia", style: defaultTextFont)),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text("3.", style: defaultTextFont.copyWith())),
                DataCell(Text("TamanSari", style: defaultTextFont.copyWith())),
                DataCell(Text("11/2", style: defaultTextFont.copyWith())),
                DataCell(Text("Sanusi", style: defaultTextFont.copyWith())),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text("4.", style: defaultTextFont.copyWith())),
                DataCell(Text("Suralaya", style: defaultTextFont.copyWith())),
                DataCell(Text("3/2", style: defaultTextFont.copyWith())),
                DataCell(Text("Suprapto", style: defaultTextFont.copyWith())),
              ],
            ),
            DataRow(
              cells: <DataCell>[
                DataCell(Text("5.", style: defaultTextFont.copyWith())),
                DataCell(Text("Lebakgede", style: defaultTextFont.copyWith())),
                DataCell(Text("11/2", style: defaultTextFont.copyWith())),
                DataCell(Text("Mahsudi", style: defaultTextFont.copyWith())),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F7F9),
      appBar: header(),
      body: Container(
        child: ListView(
          children: [
            Column(
              children: [
                tableJkn(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
