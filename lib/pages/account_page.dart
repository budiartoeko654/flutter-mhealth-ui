import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';
import 'package:m_health/widgets/card_menu_profile.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text("Akun"),
        leading: Container(),
        backgroundColor: Color(0xFF00CACD),
      );
    }

    Widget containerPhoto() {
      return Container(
        padding: EdgeInsets.only(bottom: 25),
        decoration: BoxDecoration(
          color: Color(0xFF00CACD),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(top: 30),
              width: 120,
              height: 120,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage("assets/user_pic.png"),
                    fit: BoxFit.cover),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Text(
                    "Alex Hermawan",
                    style: defaultTextFont.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Text(
                    "alex@gmail.com",
                    style: defaultTextFont.copyWith(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    Widget containerListProfile() {
      return Container(
        child: Column(
          children: [
            CardMenuProfile("assets/hospital.png", "P161015201"),
            CardMenuProfile("assets/clinic.png", "Puskesmas Betung"),
            CardMenuProfile("assets/logout.png", "Sign Out"),
          ],
        ),
      );
    }

    Widget listMenu() {
      return Container(
        child: Column(
          children: [],
        ),
      );
    }

    return Scaffold(
      appBar: header(),
      body: ListView(
        children: [
          containerPhoto(),
          listMenu(),
          containerListProfile(),
        ],
      ),
    );
  }
}
