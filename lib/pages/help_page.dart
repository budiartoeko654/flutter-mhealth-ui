import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';
import 'package:m_health/widgets/card_help_center.dart';

class HelpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return AppBar(
        elevation: 0,
        leading: Container(),
        backgroundColor: Color(0xFF00CACD),
        title: Text(
          "Bantuan",
          style: defaultTextFont.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      );
    }

    // WIDGET HEADER
    Widget headerPicture() {
      return Container(
        padding: EdgeInsets.only(bottom: 5),
        child: Container(
          width: double.infinity,
          height: 235,
          decoration: BoxDecoration(
            color: Color(0xFF00CACD),
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
            image: DecorationImage(
              image: AssetImage("assets/customer.png"),
            ),
          ),
        ),
      );
    }

    Widget containerListHelp() {
      return Container(
        child: Column(
          children: [
            CardHelp("assets/hospital.png", "Homepage Kemenkes"),
            CardHelp("assets/email.png", "Email"),
            CardHelp("assets/contacts.png", "Telp Center"),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: header(),
      body: ListView(
        children: [
          headerPicture(),
          containerListHelp(),
        ],
      ),
    );
  }
}
