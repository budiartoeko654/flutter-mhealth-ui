import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:m_health/pages/Menu/bottom_navbar_menu.dart';
import 'package:m_health/pages/news_page.dart';
import 'package:m_health/pages/news_headline_page.dart';
import 'package:m_health/pages/schedule_page.dart';
import 'package:m_health/shared/theme.dart';
import 'package:m_health/widgets/card_menu.dart';
import 'package:m_health/widgets/item_card_slider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> cardList = [
    "",
    "",
  ];

  // try a pull ruquest
  // hope this success

  @override
  Widget build(BuildContext context) {
    // FUNCTION IMAGE SLIDER

    // WIDGET HEADER
    Widget header() {
      return Container(
        padding: EdgeInsets.only(
          left: defaultMargin,
          top: 20,
          right: defaultMargin,
          bottom: 30,
        ),
        decoration: BoxDecoration(
          color: Color(0xFF00CACD),
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
        ),
        child: Row(
          children: [
            Container(
              width: 54,
              height: 54,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(
                    "assets/person.png",
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Hallo, Alex",
                    style: defaultTextFont.copyWith(
                      fontSize: 24,
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Nakes",
                    style: defaultTextFont.copyWith(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              width: 60,
              height: 80,
              child: Column(
                children: [
                  Image.asset(
                    "assets/healthcare.png",
                    width: 45,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Belitung",
                    style: defaultTextFont.copyWith(
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                      fontSize: 15,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
    }

    // WIDGET CARD SLIDER
    Widget carouselSlide() {
      return GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => NewsHeadline()));
        },
        child: Container(
          margin: EdgeInsets.only(top: 20),
          child: Center(
            child: CarouselSlider(
              options: CarouselOptions(
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                pauseAutoPlayOnTouch: true,
                enlargeCenterPage: true,
                viewportFraction: 0.8,
              ),
              items: cardList.map((item) {
                return ItemCardSlider(title: item);
              }).toList(),
            ),
          ),
        ),
      );
    }

    Widget menu() {
      return Container(
        padding: EdgeInsets.all(30),
        width: double.infinity,
        height: 380,
        margin: EdgeInsets.only(
          left: defaultMargin,
          right: defaultMargin,
          top: 35,
        ),
        decoration: BoxDecoration(
          color: Color(0xFF00CACD),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "JKN",
                    "assets/jkn_logo.jpg",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Keluarga\nBerencana",
                    "assets/keluarga.png",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Imunisasi",
                    "assets/imun.jpg",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "ASI",
                    "assets/asi.jpg",
                  ),
                )
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "TBC",
                    "assets/paru_paru.png",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Pertumbuhan\nBalita",
                    "assets/balita.jpg",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Hipertensi",
                    "assets/hipertensi.png",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Ibu\nBersalin",
                    "assets/ibu_bersalin.jpg",
                  ),
                )
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Air\nBersih",
                    "assets/air.jpg",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Gangguan\nJiwa",
                    "assets/jiwa.jpg",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Keluarga\nMerokok",
                    "assets/rokok.png",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomNavbarMenu()));
                  },
                  child: CardMenu(
                    "Jamban\nSehat",
                    "assets/toilet.png",
                  ),
                )
              ],
            )
          ],
        ),
      );
    }

    Widget cardBeritaKesehatan() {
      return GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => DetailsNews()));
        },
        child: Container(
          padding: EdgeInsets.all(15),
          margin: EdgeInsets.only(
            left: defaultMargin,
            right: defaultMargin,
            top: 30,
          ),
          width: double.infinity,
          height: 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Color(0xFF00CACD),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                margin: EdgeInsets.only(right: 20),
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color(0xFF009D9F),
                ),
                child: Icon(
                  Icons.message_outlined,
                  color: Colors.white,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Berita Kesehatan",
                      style: defaultTextFont.copyWith(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Selalu update kabar terbaru",
                      style: defaultTextFont.copyWith(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              )
            ],
          ),
        ),
      );
    }

    Widget cardJadwalBeritaKesehatan() {
      return GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => SchedulePage()));
        },
        child: Container(
          padding: EdgeInsets.all(15),
          margin: EdgeInsets.only(
            left: defaultMargin,
            right: defaultMargin,
            top: 20,
          ),
          width: double.infinity,
          height: 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Color(0xFF00CACD),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                margin: EdgeInsets.only(right: 20),
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color(0xFF009D9F),
                ),
                child: Icon(
                  Icons.calendar_today_outlined,
                  color: Colors.white,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Jadwal Kesehatan",
                      style: defaultTextFont.copyWith(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Jangan sampai salah jadwal",
                      style: defaultTextFont.copyWith(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              )
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Color(0xFFF6F7F9),
      body: ListView(
        children: [
          header(),
          carouselSlide(),
          menu(),
          cardBeritaKesehatan(),
          cardJadwalBeritaKesehatan(),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
