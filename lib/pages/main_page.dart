import 'package:flutter/material.dart';
import 'package:m_health/pages/account_page.dart';
import 'package:m_health/pages/help_page.dart';
import 'package:m_health/pages/home_page.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    Widget body() {
      switch (currentIndex) {
        case 0:
          return HomePage();
          break;
        case 1:
          return AccountPage();
          break;
        case 2:
          return HelpPage();
          break;
        default:
          return HomePage();
      }
    }

    // WIDGET CUSTOM BOTTOM NAVBAR
    Widget customButtomNavbar() {
      return BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        clipBehavior: Clip.antiAlias,
        child: BottomNavigationBar(
          currentIndex: currentIndex,
          onTap: (value) {
            setState(() {
              currentIndex = value;
            });
          },
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 20, bottom: 10),
                child: Icon(
                  Icons.home,
                  size: 30,
                  color: currentIndex == 0 ? Color(0xFF00CACD) : Colors.grey,
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 20, bottom: 10),
                child: Icon(
                  Icons.person,
                  size: 30,
                  color: currentIndex == 1 ? Color(0xFF00CACD) : Colors.grey,
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(top: 20, bottom: 10),
                child: Icon(
                  Icons.help,
                  size: 30,
                  color: currentIndex == 2 ? Color(0xFF00CACD) : Colors.grey,
                ),
              ),
              label: '',
            ),
          ],
        ),
      );
    }

    return Scaffold(
      bottomNavigationBar: customButtomNavbar(),
      body: body(),
    );
  }
}
