import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';

class NewsHeadline extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF00CACD),
        title: Text("Berita Terkini"),
        centerTitle: true,
      );
    }

    Widget imageHeader() {
      return Container(
        width: double.infinity,
        height: 200,
        margin: EdgeInsets.only(
          top: 30,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Color(0xFF00CACD),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/healthcare.png",
              width: 150,
            ),
          ],
        ),
      );
    }

    Widget titleNews() {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
        ),
        child: Text(
          "Kesehatan dan Keselamatan Peserta Tes SKD Kemenkes Menjadi Prioritas di Masa Pandemi",
          style: defaultTextFont.copyWith(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );
    }

    Widget timeNews() {
      return Container(
        margin: EdgeInsets.only(top: 15),
        child: Text(
          "Medan 27 September 2021",
          style: defaultTextFont.copyWith(
            fontSize: 16,
            color: Colors.grey,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }

    Widget news() {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
          bottom: 35,
        ),
        child: Text(
          "Hari ini (27/9) sebanyak 817 peserta tes Seleksi Kompetensi Dasar (SKD) Kementerian Kesehatan siap melaksanakan ujian di Kantor Kanreg VI Medan, Sumatera Utara yang dibagi dalam tiga sesi.Pelaksanaan tes dilakukan dengan protokol kesehatan yang ketat mulai dari pengecekan suhu, cuci tangan dengan sabun, validasi data diri, deklarasi sehat, absensi Kemenkes secara online, dilanjutkan dengan validasi face recognition yang mulai digunakan pada tahun ini.Direktur Politeknik Kesehatan Kementerian Kesehatan RI Medan Dra. Ida Nurhayati M.Kes menyampaikan persiapan sudah dilakukan selama 2 (dua) bulan sebelumnya, tahun ini Poltekkes Medan terpilih sebagai ketua penyelenggara dan RSUP H. Adam Malik sebagai wakil ketua penyelenggara.“Seluruh UPT vertikal terlibat didalamnya,",
          style: defaultTextFont.copyWith(
            fontSize: 16,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: header(),
      body: Container(
        margin: EdgeInsets.only(
          left: defaultMargin,
          right: defaultMargin,
        ),
        child: ListView(
          children: [
            imageHeader(),
            titleNews(),
            timeNews(),
            news(),
          ],
        ),
      ),
    );
  }
}
