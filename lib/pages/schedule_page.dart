import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';
import 'package:m_health/widgets/card_schedule.dart';

class SchedulePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF00CACD),
        title: Text(
          "Jadwal Kegiatan",
          style: defaultTextFont.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      );
    }

    return Scaffold(
      appBar: header(),
      body: ListView(
        children: [
          CardSchedule("JUL", "12", "26 JULI 2021", "PPKM 2021", "Pertemuan"),
          CardSchedule("APR", "21", "21 APRIL 2021",
              "Peringatan Hari AIDS Sedunia", "Peringatan"),
          CardSchedule("FEB", "25", "25 FEBRUARI 2021", "Vaksinasi Covid",
              "Sosialisasi"),
          CardSchedule(
              "DES", "12", "12 DESEMBER 2021", "Mhealt Cilodong", "Posyandu"),
          CardSchedule("DES", "15", "15 DESEMBER 2021", "Sosialisasi Folio",
              "Sosialisasi"),
        ],
      ),
    );
  }
}
