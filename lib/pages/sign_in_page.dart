import 'package:flutter/material.dart';
import 'package:m_health/pages/main_page.dart';
import 'package:m_health/shared/theme.dart';

class SignInPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    TextEditingController provinsiController = TextEditingController();

    // WIDGET ICON HEADER
    Widget iconHeader() {
      return Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Image.asset(
              "assets/healthcare.png",
              width: 100,
            ),
          ),
        ],
      );
    }

    // WIDGET HEADER
    Widget header() {
      return Container(
        margin: EdgeInsets.only(
          top: 30,
          bottom: 30,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Login',
              style: defaultTextFont.copyWith(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF0C96C5),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Sign In To Continue',
              style: defaultTextFont.copyWith(
                fontSize: 15,
                color: Color(0xFF0C96C5),
              ),
            )
          ],
        ),
      );
    }

    // WIDGET EMAIL CONTROLLER
    Widget inputEmail() {
      return Container(
        child: TextFormField(
          controller: emailController,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            labelText: "Email Address",
            hintText: "Email Address",
            // hintStyle: defaultTextFont.copyWith(
            //   fontSize: 16,
            //   color: Color(0xff0C96C5),
            // ),
            labelStyle: defaultTextFont.copyWith(
              fontSize: 16,
              color: Color(0xff0C96C5),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color: Color(0xFF0C96C5),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color: Color(0xff0C96C5),
              ),
            ),
          ),
        ),
      );
    }

    // WIDGET PASSWORD INPUT
    Widget inputPassword() {
      return Container(
        child: TextField(
          controller: passwordController,
          obscureText: true,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            labelText: "Password",
            hintText: "Password",
            // hintStyle: defaultTextFont.copyWith(
            //   fontSize: 16,
            //   color: Color(0xff00CACD),
            // ),
            labelStyle: defaultTextFont.copyWith(
              fontSize: 16,
              color: Color(0xff0C96C5),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color: Color(0xFF0C96C5),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color: Color(0xff0C96C5),
              ),
            ),
          ),
        ),
      );
    }

    // WIDGET PROVINSI INPUT
    Widget inputProvinsi() {
      return Container(
        child: TextFormField(
          controller: provinsiController,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            labelText: "Provinsi",
            hintText: "Provinsi",
            // hintStyle: defaultTextFont.copyWith(
            //   fontSize: 16,
            //   color: Color(0xff0C96C5),
            // ),
            labelStyle: defaultTextFont.copyWith(
              fontSize: 16,
              color: Color(0xff0C96C5),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color: Color(0xFF0C96C5),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color: Color(0xff0C96C5),
              ),
            ),
          ),
        ),
      );
    }

    // WIDGET SIGN IN BUTTON
    Widget signInButton() {
      return Container(
        height: 50,
        width: double.infinity,
        margin: EdgeInsets.only(
          top: 30,
        ),
        child: TextButton(
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (_) => MainPage()));
          },
          style: TextButton.styleFrom(
            backgroundColor: Color(0xFF0C96C5),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
          ),
          child: Text(
            "Sign In",
            style: defaultTextFont.copyWith(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      );
    }

    return Scaffold(
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: defaultMargin,
        ),
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                iconHeader(),
                SizedBox(
                  height: 20,
                ),
                header(),
                inputEmail(),
                SizedBox(
                  height: 20,
                ),
                inputPassword(),
                SizedBox(
                  height: 20,
                ),
                inputProvinsi(),
                signInButton(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
