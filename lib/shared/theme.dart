import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const double defaultMargin = 24;

TextStyle defaultTextFont = GoogleFonts.raleway();
