import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';

class CardMenu extends StatelessWidget {
  final String title;
  final String image;

  CardMenu(this.title, this.image);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            width: 45,
            height: 45,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage(
                    image,
                  ),
                  fit: BoxFit.cover,
                )),
          ),
          SizedBox(
            height: 7,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: defaultTextFont.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
          )
        ],
      ),
    );
  }
}
