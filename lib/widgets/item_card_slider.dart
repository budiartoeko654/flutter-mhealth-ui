import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';

class ItemCardSlider extends StatelessWidget {
  final String title;

  ItemCardSlider({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(
            left: 5,
            right: 5,
            top: 30,
          ),
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.height * 0.6,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Colors.grey),
            image: DecorationImage(
              image: AssetImage(
                "assets/healthcare.png",
              ),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Center(
                child: Container(
                  padding: EdgeInsets.only(top: 10),
                  width: double.infinity,
                  height: 100,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.white60,
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset: Offset(0, 0),
                      ),
                    ],
                  ),
                  child: Text(
                    "Kunjungi B2P2TOOT Sekjen: Perlu adanya Transformasi di Bidang Kesehatan",
                    style: defaultTextFont.copyWith(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
