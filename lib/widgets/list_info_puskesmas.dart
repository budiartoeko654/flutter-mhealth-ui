import 'package:flutter/material.dart';
import 'package:m_health/shared/theme.dart';

class ListInfoPuskesmas extends StatelessWidget {
  final String title;
  final String description;

  ListInfoPuskesmas(this.title, this.description);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: defaultTextFont.copyWith(
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
        Text(
          description,
          style: defaultTextFont.copyWith(
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}
